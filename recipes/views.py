from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
# Create your views here.


def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)

    context = {
        'recipe_object': recipe,
    }
    return render(request, 'recipes/detail.html', context)


def recipe_list(request):
    recipes = Recipe.objects.all()

    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)


def create_recipe(request):
    if request.method == "POST":
        # we should use the form to validate the values
        # and save them to the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            # If all goes well, we can redirect the browser
            # to another page and leave the function
            # in this case we are going to redirect the page
            # to "recipe_list"
            return redirect("recipe_list")
    else:
        # create an instance of the django model form class
        # we made a class called RecipeForm in the form.py
        form = RecipeForm()

    # put the form in the context
    context = {
        "form": form,
    }

    # render the HTML template with the form
    return render(request, "recipes/create.html", context)


def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id,)
    # Get the object that we want to edit
    if request.method == "POST":
        # POST is when the person has submitted the form
        # We should use the form to validate the values
        #   and save them to the database
        # If all goes well, we can redirect the browser
        #   to another page
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            recipe = form.save()
            return redirect("show_recipe", id=id)
    else:
        # Create an instance of the Django model form class
        #   with the object that the person wants to edit
        form = RecipeForm(instance=recipe)
    # Put the form in the context
    # Render the HTML template with the form
    context = {
        "recipe": recipe,
        "form": form,
    }
    return render(request, "recipes/edit.html", context)
